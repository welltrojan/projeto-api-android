package com.example.wellington.projetoapi

import android.content.Context
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter


class GitHubRepoAdapter(val mCtx: Context, val values: List<GitHubRepo>) : ArrayAdapter<GitHubRepo>(mCtx, R.layout.list_item_pagination, values) {

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var row: View? = convertView

        if (row == null) {
            val inflater = mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            row = inflater.inflate(R.layout.list_item_pagination, parent, false)
        }

        val textView = row!!.findViewById(R.id.list_item_pagination_text) as TextView

        val item = values[position]
        val message = item.getName()
        textView.text = message

        return row
    }
}